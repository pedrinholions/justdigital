import {Injectable} from '@angular/core';
import {Product} from '../_models/product';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  obs = new Subject();
  cartItems: Product[];
  totalPrice: number;

  constructor() {
    this.cartItems = [];
  }

  public getCartItems(): Subject<any> {
    return this.obs;
  }

  public add(product: Product): void {
    /* verificar se o objeto já tem quantidade no carrinho */
    if (!product.quantityInCart) {
      product.quantityInCart = 1;
    }

    let productExist = false;

    this.cartItems = this.cartItems.map((item) => {
      // verifica se o produto já existe no carrinho
      if (item.id === product.id) {
        productExist = true;
        // se a quantidade no carrinho for menor que o estoque, pode adicionar
        if (item.quantityInCart < product.quantity) {
          item.quantityInCart++;
          /*
            se a quantidade no carrinho for igual ao estoque,
            avisa que não tem mais estoque para a quantidade desejada
           */
        } else if (item.quantityInCart === item.quantity) {
          // disparar alerta
          alert(`Só temos ${item.quantity} desse item no estoque`);
        }
      }

      return item;
    });

    if (!productExist) {
      this.cartItems.push(product);
    }

    this.update(this.cartItems);
  }

  public remove(id, removeOne = false) {
    this.cartItems.forEach((item, index) => {
      if (item.id === id) {
        if (removeOne) {
          // Se a quantidade for 1, deleta do array
          if (item.quantityInCart === 1) {
            this.cartItems.splice(index, 1);
          } else {
            // Se não, decrementa da quantidade no carrinho
            this.cartItems[index].quantityInCart--;
          }
        } else {
          // deletar todos do carrinho
          this.cartItems.splice(index, 1);
        }
      }
    });
    this.update(this.cartItems);
  }

  public update(products: Product[]): void {
    this.obs.next(products);
    this.calcPrice();
  }

  public calcPrice() {
    this.totalPrice = 0;
    this.cartItems.forEach((item) => this.totalPrice += item.price * item.quantityInCart);
  }
}
