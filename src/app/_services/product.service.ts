import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  readonly API = `https://api-desafio-front.justdigital.com.br`;

  constructor(
    private http: HttpClient
  ) {}

  public getProducts(): Observable<any> {
    return this.http.get(this.API);
  }
}
