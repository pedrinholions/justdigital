export interface Product {
  id: number;
  title: string;
  price: number;
  picture: string;
  description: string;
  memory: string;
  brand: string;
  chipType: string;
  quantity: number;
  quantityInCart: number;
}
