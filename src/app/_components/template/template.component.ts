import { Component, OnInit } from '@angular/core';


declare var $: any;

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})
export class TemplateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openDropdown() {
    $('#dropdownMenu').toggle();
  }

}
