import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../../_services/product.service';
import {Product} from '../../../_models/product';
import {CartService} from '../../../_services/cart.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

  products: Product[];

  constructor(
    public productService: ProductService,
    public cartService: CartService
  ) {
  }

  ngOnInit() {
    /* load products from API */
    this.getProducts();
  }

  getProducts(): void {
    this.productService.getProducts()
      .subscribe(response => {
          this.products = response.products;
      });
  }

  addInCart(item): void {
    this.cartService.add(item);
  }

}
