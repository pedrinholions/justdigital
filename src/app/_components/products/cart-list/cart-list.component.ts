import {Component, OnInit} from '@angular/core';
import {CartService} from '../../../_services/cart.service';
import {Product} from '../../../_models/product';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.scss']
})
export class CartListComponent implements OnInit {

  cartItems = new Array<Product[]>();

  constructor(
    public cartService: CartService
  ) {
  }

  ngOnInit(): void {
    this.cartService.getCartItems().subscribe(items => {
      this.cartItems = items;
    });
  }

  add(product: Product) {
    this.cartService.add(product);
  }

  remove(id: number, removeOne: boolean): void {
    this.cartService.remove(id, removeOne);
  }

  transformTitle(title: string): string {
    return `${title.slice(0, 30)}...`;
  }

}
