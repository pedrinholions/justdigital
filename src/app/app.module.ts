import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplateComponent } from './_components/template/template.component';
import { ItemListComponent } from './_components/products/item-list/item-list.component';
import {HttpClientModule} from '@angular/common/http';
import { CartListComponent } from './_components/products/cart-list/cart-list.component';
import { CartItemComponent } from './_components/products/cart-item/cart-item.component';

@NgModule({
  declarations: [
    AppComponent,
    TemplateComponent,
    ItemListComponent,
    CartListComponent,
    CartItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
